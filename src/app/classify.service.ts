import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  res(res: any) {
    throw new Error('Method not implemented.');
  }
  private url = " https://y6rgn7pyjh.execute-api.us-east-1.amazonaws.com/beta";

  categories:object = {0:'Business', 1:'Entertainment', 2:'Politics', 3:'Sport', 4:'Tech'};


list2=[];


  classify(text:string){
    this.list2=[text];
    let json = {'articles':[
      {'text':text}
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        this.list2.push(res);
        return res;
      })
    );
   

  }


  constructor(private http:HttpClient) { }
}