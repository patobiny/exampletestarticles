import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Article } from '../interfaces/article';
import { AuthService } from '../auth.service';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  text:string;
  category:string;
  list=[];
  options=["Entertainment","Sport","Etc"]

    show(){
     this.list=this.classifyService.list2;
     this.text=this.list[0];
     this.category=this.classifyService.categories[this.classifyService.list2[1]] ;

      
       }


       articles:[];
       articles$;
       userId:string;
       addCustomerFormOpen= false;
     
     
       id:string;
       constructor(public authService:AuthService,private articleService:ArticlesService,private classifyService:ClassifyService) { }
       add(user,text,category){
         console.log(this.text);
         this.articleService.addArticle(this.userId, this.text, this.category);
       }


  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
  })}
  

}
