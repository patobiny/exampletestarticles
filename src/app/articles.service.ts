import { AngularFirestoreModule, AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  articlesCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection=this.db.collection('users');

  getArticles(userId):Observable<any[]>{
    this.articlesCollection = this.db.collection(`users/${userId}/articles`);
    return this.articlesCollection.snapshotChanges().pipe(
      map(
        collection =>collection.map(
          document=> {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data; 
          }
        )  
     ))  
  }

  addArticle(userId:string, text:string,category){
    console.log(category);
    const article = {text:text,category:category};
    this.userCollection.doc(userId).collection('articles').add(article);
  }

  constructor(private db:AngularFirestore) { }
}
