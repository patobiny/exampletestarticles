import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import { AuthService } from '../auth.service';
import { Article } from '../interfaces/article';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {


  articles:[];
  articles$;
  userId:string;
  addCustomerFormOpen= false;

  text:string;
  category:string;
  id:string;
  constructor(public authService:AuthService,private articleService:ArticlesService) { }
  add(article:Article){
    this.articleService.addArticle(this.userId, article.text, article.category);
  }
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.articles$ = this.articleService.getArticles(this.userId);
        
    })
  }

}
