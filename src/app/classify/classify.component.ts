import { Component, OnInit } from '@angular/core';
import { ClassifyService } from './../classify.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  text:string; 
  category:string = 'No category';

  
  classify(){
    this.classifyService.classify(this.text).subscribe(
      res => {
        console.log(res);
        this.category = this.classifyService.categories[res];
        this.router.navigate(['/welcome']);
      }
    )


  }

  constructor(private route:ActivatedRoute, 
              private classifyService:ClassifyService,
              private router:Router) { }

  ngOnInit(): void {
  }

}





  


